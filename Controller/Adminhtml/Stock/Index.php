<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Controller\Adminhtml\Stock;

class Index extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;
    /**
     * @var \Magento\CatalogInventory\Api\StockStateInterface
     */
    protected $_stockStateInterface;
    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $_stockRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Kowal\UpdateStock\Lib\Stock $stock
     */
    public function __construct(
        \Magento\Backend\App\Action\Context                  $context,
        \Magento\Framework\View\Result\PageFactory           $resultPageFactory,
        \Magento\Catalog\Model\Product                       $product,
        \Magento\Catalog\Model\ProductRepository             $productRepository,
        \Magento\CatalogInventory\Api\StockStateInterface    $stockStateInterface,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Kowal\UpdateStock\Lib\Stock                         $stock
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->_product = $product;
        $this->_productRepository = $productRepository;
        $this->_stockStateInterface = $stockStateInterface;
        $this->_stockRegistry = $stockRegistry;
        $this->stock = $stock;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $data = $this->getArrayForImport();
        if ($data) {

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $modelPrzyjecie = $objectManager->create(\Kowal\UpdateStock\Model\PrzyjecieMagazynowe::class)->load(null);
            $dataPz = ['date' => date('Y-m-d H:i:s'), 'numer' => $this->getRequest()->getParam('numer', null), 'uwagi' => $this->getRequest()->getParam('uwagi', null), 'magazyn' => $this->getRequest()->getParam('source', null)];

            $modelPrzyjecie->setData($dataPz);
            $modelPrzyjecie->save();
            $document_id = $modelPrzyjecie->getId();

            foreach ($data as $p) {
                $this->updateProductStock($p['sku'], $p['qty']);
                $this->createPrzyjecie($p['sku'], $p['qty'], $document_id, $objectManager);
            }
            $this->messageManager->addSuccessMessage(__('Dodano nowy dokument PM'));
            return $resultRedirect->setPath('kowal_updatestock/przyjeciemagazynowe/index');
        }


        return $this->resultPageFactory->create();
    }


    private function getArrayForImport()
    {
        $data = $this->getRequest()->getParam('skus', null);

        if ($data) {
            $data = array_filter(array_map('trim', explode(PHP_EOL, $data)));

            $data = array_map(
                function ($row) {
                    if (preg_match("/\t/", $row)) {
                        $row = explode('	', $row);
                    } else {
                        $row = explode(',', $row);
                    }
                    $row['sku'] = $row[0] ?? '';
                    unset($row[0]);

                    $qty = 1;
                    if (isset($row[1])) {
                        $possibleQty = explode(':', $row[1]);
                        if ((count($possibleQty) === 1 && (float)$possibleQty) || $possibleQty[0] === 'qty') {
                            $qty = (float)end($possibleQty);
                            unset($row[1]);
                        }
                    }
                    $row['qty'] = $qty;

                    return $row;
                },
                $data
            );
            $this->totalQty = count($data);
        }

        return $data;
    }


    public function updateProductStock($sku, $qty)
    {


//        $product_ = $this->getProductBySku($sku);
        $source_code = (string)$this->getRequest()->getParam('source', null);

//        $stockItem = $this->_stockRegistry->getStockItem($product_->getEntityId()); // load stock of that product
//        $newQty = (float)$stockItem->getData('qty') + (float)$qty;

//        $stock = $this->getSourceItemQtyBySKU($sku, $source_code);
//        $newQty = $stock + (float)$qty;
//
//
//        $sourceItem = $this->sourceItemFactory->create();
//        $sourceItem->setSourceCode($source_code);
//        $sourceItem->setSku($sku);
//        $sourceItem->setQuantity($newQty);
//        $sourceItem->setStatus(1);
//        $this->sourceItemsSaveInterface->execute([$sourceItem]);


        $this->stock->execute($sku, (float)$qty, $source_code, true);
    }

    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }

    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }


    private function createPrzyjecie($sku, $qty, $id, $objectManager)
    {
        $product = $this->getProductBySku($sku);
        $modelItems = $objectManager->create(\Kowal\UpdateStock\Model\Przyjecie::class)->load(null);

        $dataItens = ['date' => date('Y-m-d H:i:s'), 'sku' => $sku, "name" => $product->getName(), 'ilosc' => $qty, 'magazyn' => $this->getRequest()->getParam('source', null), "przyjecie_magazynowe_id" => $id];
        $modelItems->setData($dataItens);
        $modelItems->save();
        return $modelItems->getId();
    }

    public function getSourceItemQtyBySKU($sku, $magazyn)
    {
        $qty = 0;
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(\Magento\InventoryApi\Api\Data\SourceItemInterface::SKU, $sku)
            ->create();

        $result = $this->sourceItemRepositoryInterface->getList($searchCriteria)->getItems();

        foreach ($result as $item) {

            // print_r($item->getData());
            /*
                [source_item_id] => 7
                [source_code] => default
                [sku] => 24-UB02
                [quantity] => 98.0000
                [status] => 1
             */

            if (isset($item['source_code']) && $item['source_code'] == $magazyn) {
                $qty = $item['quantity'];
            }
        }

        return $qty;
    }

}

