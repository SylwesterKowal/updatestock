<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Controller\Adminhtml\PrzyjecieMagazynowe;

class Edit extends \Kowal\UpdateStock\Controller\Adminhtml\PrzyjecieMagazynowe
{

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('przyjeciemagazynowe_id');
        $model = $this->_objectManager->create(\Kowal\UpdateStock\Model\PrzyjecieMagazynowe::class);

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('Dokument nie istnieje'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('kowal_updatestock_przyjeciemagazynowe', $model);

        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Przyjeciemagazynowe') : __('Nowe przyjęcie magazynowe'),
            $id ? __('Edit Przyjeciemagazynowe') : __('Nowe przyjęcie magazynowe')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Przyjęcie Magazynowe'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Podgląd Przyjecia Magazynowego %1', $model->getId()." / ".$model->getNumer()) : __('Nowe przyjęcie magazynowe'));
        return $resultPage;
    }
}
