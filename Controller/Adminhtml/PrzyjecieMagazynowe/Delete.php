<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Controller\Adminhtml\PrzyjecieMagazynowe;

class Delete extends \Kowal\UpdateStock\Controller\Adminhtml\PrzyjecieMagazynowe
{

    public function __construct(
        \Magento\Backend\App\Action\Context                                $context,
        \Magento\Framework\Registry                                        $coreRegistry,
        \Kowal\UpdateStock\Lib\Stock                                       $stock,
        \Kowal\UpdateStock\Model\ResourceModel\Przyjecie\CollectionFactory $collectionFactoryPrzyjecie
    )
    {
        $this->stock = $stock;
        $this->collectionFactoryPrzyjecie = $collectionFactoryPrzyjecie;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('przyjeciemagazynowe_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Kowal\UpdateStock\Model\PrzyjecieMagazynowe::class);
                $model->load($id);
                $model->delete();

                $przyjecieCollection = $this->collectionFactoryPrzyjecie->create();
                $przyjecieCollection->addFieldToFilter('przyjecie_magazynowe_id', array('eq' => $id));
                $collectionItems =  $przyjecieCollection->addFieldToSelect('*');

                foreach ($collectionItems as $item){
                    $sku = $item->getSku();
                    $ilosc = $item->getIlosc();
                    $magazyn = $item->getMagazyn();

                    $modelItem = $this->_objectManager->create(\Kowal\UpdateStock\Model\Przyjecie::class);
                    $modelItem->load($item->getId());
                    $modelItem->delete();
                    $this->stock->execute($sku, $ilosc, $magazyn, false);
                }

                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Przyjeciemagazynowe.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['przyjeciemagazynowe_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Przyjeciemagazynowe to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
