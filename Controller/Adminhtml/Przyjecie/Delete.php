<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Controller\Adminhtml\Przyjecie;

class Delete extends \Kowal\UpdateStock\Controller\Adminhtml\Przyjecie
{

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry         $coreRegistry,
//        \Magento\InventoryApi\Api\SourceItemsSaveInterface        $sourceItemsSaveInterface,
//        \Magento\InventoryApi\Api\Data\SourceItemInterfaceFactory $sourceItemFactory,
//        \Magento\Framework\Api\SearchCriteriaBuilder              $searchCriteriaBuilder,
//        \Magento\InventoryApi\Api\Data\SourceItemInterface        $sourceItemInterface,
//        \Magento\InventoryApi\Api\SourceItemRepositoryInterface   $sourceItemRepositoryInterface,
        \Kowal\UpdateStock\Lib\Stock        $stock
    )
    {
//        $this->sourceItemsSaveInterface = $sourceItemsSaveInterface;
//        $this->sourceItemFactory = $sourceItemFactory;
//        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
//        $this->sourceItemInterface = $sourceItemInterface;
//        $this->sourceItemRepositoryInterface = $sourceItemRepositoryInterface;

        $this->stock = $stock;

        parent::__construct($context, $coreRegistry);
    }


    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('przyjecie_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Kowal\UpdateStock\Model\Przyjecie::class);
                $model->load($id);
                $model->delete();

                $sku = $model->getSku();
                $ilosc = $model->getIlosc();
                $magazyn = $model->getMagazyn();

//                $stock = $this->getSourceItemQtyBySKU($sku, $magazyn);
//                $newQty = $stock - $ilosc;
//
//                $sourceItem = $this->sourceItemFactory->create();
//                $sourceItem->setSourceCode($magazyn);
//                $sourceItem->setSku($sku);
//                $sourceItem->setQuantity($newQty);
//                $sourceItem->setStatus(1);
//                $this->sourceItemsSaveInterface->execute([$sourceItem]);

                $this->stock->execute($sku, $ilosc, $magazyn, false);


                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Przyjecie.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['przyjecie_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Przyjecie to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }


    public function getSourceItemQtyBySKU($sku, $magazyn)
    {
        $qty = 0;
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(\Magento\InventoryApi\Api\Data\SourceItemInterface::SKU, $sku)
            ->create();

        $result = $this->sourceItemRepositoryInterface->getList($searchCriteria)->getItems();

        foreach ($result as $item) {

            // print_r($item->getData());
            /*
                [source_item_id] => 7
                [source_code] => default
                [sku] => 24-UB02
                [quantity] => 98.0000
                [status] => 1
             */

            if (isset($item['source_code']) && $item['source_code'] == $magazyn) {
                $qty = $item['quantity'];
            }
        }

        return $qty;
    }
}

