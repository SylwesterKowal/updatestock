<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Api\Data;

interface PrzyjecieSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Przyjecie list.
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface[]
     */
    public function getItems();

    /**
     * Set date list.
     * @param \Kowal\UpdateStock\Api\Data\PrzyjecieInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

