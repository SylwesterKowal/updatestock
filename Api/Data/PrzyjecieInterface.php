<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Api\Data;

interface PrzyjecieInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const ILOSC = 'ilosc';
    const SKU = 'sku';
    const PRZYJECIE_ID = 'przyjecie_id';
    const DATE = 'date';
    const MAGAZYN = 'magazyn';

    /**
     * Get przyjecie_id
     * @return string|null
     */
    public function getPrzyjecieId();

    /**
     * Set przyjecie_id
     * @param string $przyjecieId
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface
     */
    public function setPrzyjecieId($przyjecieId);

    /**
     * Get date
     * @return string|null
     */
    public function getDate();

    /**
     * Set date
     * @param string $date
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface
     */
    public function setDate($date);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Kowal\UpdateStock\Api\Data\PrzyjecieExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\UpdateStock\Api\Data\PrzyjecieExtensionInterface $extensionAttributes
    );

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku
     * @param string $sku
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface
     */
    public function setSku($sku);

    /**
     * Get ilosc
     * @return string|null
     */
    public function getIlosc();

    /**
     * Set ilosc
     * @param string $ilosc
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface
     */
    public function setIlosc($ilosc);

    /**
     * Get magazyn
     * @return string|null
     */
    public function getMagazyn();

    /**
     * Set magazyn
     * @param string $magazyn
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface
     */
    public function setMagazyn($magazyn);
}
