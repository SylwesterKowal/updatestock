<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Api\Data;

interface PrzyjecieMagazynoweInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const PRZYJECIEMAGAZYNOWE_ID = 'przyjeciemagazynowe_id';
    const UWAGI = 'uwagi';
    const DATE = 'date';
    const NUMER = 'numer';
    const MAGAZYN = 'magazyn';

    /**
     * Get przyjeciemagazynowe_id
     * @return string|null
     */
    public function getPrzyjeciemagazynoweId();

    /**
     * Set przyjeciemagazynowe_id
     * @param string $przyjeciemagazynoweId
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface
     */
    public function setPrzyjeciemagazynoweId($przyjeciemagazynoweId);

    /**
     * Get numer
     * @return string|null
     */
    public function getNumer();

    /**
     * Set numer
     * @param string $numer
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface
     */
    public function setNumer($numer);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweExtensionInterface $extensionAttributes
    );

    /**
     * Get date
     * @return string|null
     */
    public function getDate();

    /**
     * Set date
     * @param string $date
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface
     */
    public function setDate($date);

    /**
     * Get uwagi
     * @return string|null
     */
    public function getUwagi();

    /**
     * Set uwagi
     * @param string $uwagi
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface
     */
    public function setUwagi($uwagi);

    /**
     * Get magazyn
     * @return string|null
     */
    public function getMagazyn();

    /**
     * Set magazyn
     * @param string $magazyn
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface
     */
    public function setMagazyn($magazyn);
}
