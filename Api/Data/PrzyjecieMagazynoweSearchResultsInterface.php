<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Api\Data;

interface PrzyjecieMagazynoweSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get PrzyjecieMagazynowe list.
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface[]
     */
    public function getItems();

    /**
     * Set numer list.
     * @param \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
