<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PrzyjecieRepositoryInterface
{

    /**
     * Save Przyjecie
     * @param \Kowal\UpdateStock\Api\Data\PrzyjecieInterface $przyjecie
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\UpdateStock\Api\Data\PrzyjecieInterface $przyjecie
    );

    /**
     * Retrieve Przyjecie
     * @param string $przyjecieId
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($przyjecieId);

    /**
     * Retrieve Przyjecie matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Przyjecie
     * @param \Kowal\UpdateStock\Api\Data\PrzyjecieInterface $przyjecie
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\UpdateStock\Api\Data\PrzyjecieInterface $przyjecie
    );

    /**
     * Delete Przyjecie by ID
     * @param string $przyjecieId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($przyjecieId);
}

