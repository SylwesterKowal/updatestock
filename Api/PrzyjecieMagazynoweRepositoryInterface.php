<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PrzyjecieMagazynoweRepositoryInterface
{

    /**
     * Save PrzyjecieMagazynowe
     * @param \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface $przyjecieMagazynowe
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface $przyjecieMagazynowe
    );

    /**
     * Retrieve PrzyjecieMagazynowe
     * @param string $przyjeciemagazynoweId
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($przyjeciemagazynoweId);

    /**
     * Retrieve PrzyjecieMagazynowe matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete PrzyjecieMagazynowe
     * @param \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface $przyjecieMagazynowe
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface $przyjecieMagazynowe
    );

    /**
     * Delete PrzyjecieMagazynowe by ID
     * @param string $przyjeciemagazynoweId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($przyjeciemagazynoweId);
}
