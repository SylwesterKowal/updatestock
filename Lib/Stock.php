<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Lib;

class Stock
{

    public function __construct(
        \Magento\InventoryApi\Api\SourceItemsSaveInterface        $sourceItemsSaveInterface,
        \Magento\InventoryApi\Api\Data\SourceItemInterfaceFactory $sourceItemFactory,
        \Magento\Framework\Api\SearchCriteriaBuilder              $searchCriteriaBuilder,
        \Magento\InventoryApi\Api\Data\SourceItemInterface        $sourceItemInterface,
        \Magento\InventoryApi\Api\SourceItemRepositoryInterface   $sourceItemRepositoryInterface
    )
    {
        $this->sourceItemsSaveInterface = $sourceItemsSaveInterface;
        $this->sourceItemFactory = $sourceItemFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sourceItemInterface = $sourceItemInterface;
        $this->sourceItemRepositoryInterface = $sourceItemRepositoryInterface;
    }


    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute($sku, $ilosc, $magazyn, $dodaj = true)
    {
        try {

            $stock = $this->getSourceItemQtyBySKU($sku, $magazyn);
            $newQty = ($dodaj) ? $stock + $ilosc : $stock - $ilosc;

            $sourceItem = $this->sourceItemFactory->create();
            $sourceItem->setSourceCode($magazyn);
            $sourceItem->setSku($sku);
            $sourceItem->setQuantity($newQty);
            $sourceItem->setStatus(1);
            $this->sourceItemsSaveInterface->execute([$sourceItem]);


        } catch (\Exception $e) {
            // display error message
            $this->messageManager->addErrorMessage($e->getMessage());
        }
    }


    public function getSourceItemQtyBySKU($sku, $magazyn)
    {
        $qty = 0;
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(\Magento\InventoryApi\Api\Data\SourceItemInterface::SKU, $sku)
            ->create();

        $result = $this->sourceItemRepositoryInterface->getList($searchCriteria)->getItems();

        foreach ($result as $item) {

            // print_r($item->getData());
            /*
                [source_item_id] => 7
                [source_code] => default
                [sku] => 24-UB02
                [quantity] => 98.0000
                [status] => 1
             */

            if (isset($item['source_code']) && $item['source_code'] == $magazyn) {
                $qty = $item['quantity'];
            }
        }

        return $qty;
    }
}

