<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Block\Adminhtml\Przyjecie;

class View extends \Magento\Backend\Block\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context                            $context,
        array                                                              $data = [],
        \Magento\Framework\Registry                                        $coreRegistry,
        \Kowal\UpdateStock\Model\ResourceModel\Przyjecie\CollectionFactory $collectionFactoryPrzyjecie
    )
    {
        $this->coreRegistry = $coreRegistry;
        $this->collectionFactoryPrzyjecie = $collectionFactoryPrzyjecie;
        parent::__construct($context, $data);
    }

    public function getItems()
    {
        $pm_id = $this->coreRegistry->registry('kowal_updatestock_przyjeciemagazynowe')->getId();
        $przyjecieCollection = $this->collectionFactoryPrzyjecie->create();
        $przyjecieCollection->addFieldToFilter('przyjecie_magazynowe_id', array('eq' => $pm_id));

        return $przyjecieCollection->addFieldToSelect('*');
    }
}

