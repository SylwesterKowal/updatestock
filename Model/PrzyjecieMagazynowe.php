<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Model;

use Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface;
use Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class PrzyjecieMagazynowe extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'kowal_updatestock_przyjeciemagazynowe';
    protected $przyjeciemagazynoweDataFactory;

    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PrzyjecieMagazynoweInterfaceFactory $przyjeciemagazynoweDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Kowal\UpdateStock\Model\ResourceModel\PrzyjecieMagazynowe $resource
     * @param \Kowal\UpdateStock\Model\ResourceModel\PrzyjecieMagazynowe\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PrzyjecieMagazynoweInterfaceFactory $przyjeciemagazynoweDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Kowal\UpdateStock\Model\ResourceModel\PrzyjecieMagazynowe $resource,
        \Kowal\UpdateStock\Model\ResourceModel\PrzyjecieMagazynowe\Collection $resourceCollection,
        array $data = []
    ) {
        $this->przyjeciemagazynoweDataFactory = $przyjeciemagazynoweDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve przyjeciemagazynowe model with przyjeciemagazynowe data
     * @return PrzyjecieMagazynoweInterface
     */
    public function getDataModel()
    {
        $przyjeciemagazynoweData = $this->getData();

        $przyjeciemagazynoweDataObject = $this->przyjeciemagazynoweDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $przyjeciemagazynoweDataObject,
            $przyjeciemagazynoweData,
            PrzyjecieMagazynoweInterface::class
        );

        return $przyjeciemagazynoweDataObject;
    }
}
