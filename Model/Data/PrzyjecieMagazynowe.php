<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Model\Data;

use Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface;

class PrzyjecieMagazynowe extends \Magento\Framework\Api\AbstractExtensibleObject implements PrzyjecieMagazynoweInterface
{

    /**
     * Get przyjeciemagazynowe_id
     * @return string|null
     */
    public function getPrzyjeciemagazynoweId()
    {
        return $this->_get(self::PRZYJECIEMAGAZYNOWE_ID);
    }

    /**
     * Set przyjeciemagazynowe_id
     * @param string $przyjeciemagazynoweId
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface
     */
    public function setPrzyjeciemagazynoweId($przyjeciemagazynoweId)
    {
        return $this->setData(self::PRZYJECIEMAGAZYNOWE_ID, $przyjeciemagazynoweId);
    }

    /**
     * Get numer
     * @return string|null
     */
    public function getNumer()
    {
        return $this->_get(self::NUMER);
    }

    /**
     * Set numer
     * @param string $numer
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface
     */
    public function setNumer($numer)
    {
        return $this->setData(self::NUMER, $numer);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get date
     * @return string|null
     */
    public function getDate()
    {
        return $this->_get(self::DATE);
    }

    /**
     * Set date
     * @param string $date
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface
     */
    public function setDate($date)
    {
        return $this->setData(self::DATE, $date);
    }

    /**
     * Get uwagi
     * @return string|null
     */
    public function getUwagi()
    {
        return $this->_get(self::UWAGI);
    }

    /**
     * Set uwagi
     * @param string $uwagi
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface
     */
    public function setUwagi($uwagi)
    {
        return $this->setData(self::UWAGI, $uwagi);
    }

    /**
     * Get magazyn
     * @return string|null
     */
    public function getMagazyn()
    {
        return $this->_get(self::MAGAZYN);
    }

    /**
     * Set magazyn
     * @param string $magazyn
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface
     */
    public function setMagazyn($magazyn)
    {
        return $this->setData(self::MAGAZYN, $magazyn);
    }
}
