<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Model\Data;

use Kowal\UpdateStock\Api\Data\PrzyjecieInterface;

class Przyjecie extends \Magento\Framework\Api\AbstractExtensibleObject implements PrzyjecieInterface
{

    /**
     * Get przyjecie_id
     * @return string|null
     */
    public function getPrzyjecieId()
    {
        return $this->_get(self::PRZYJECIE_ID);
    }

    /**
     * Set przyjecie_id
     * @param string $przyjecieId
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface
     */
    public function setPrzyjecieId($przyjecieId)
    {
        return $this->setData(self::PRZYJECIE_ID, $przyjecieId);
    }

    /**
     * Get date
     * @return string|null
     */
    public function getDate()
    {
        return $this->_get(self::DATE);
    }

    /**
     * Set date
     * @param string $date
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface
     */
    public function setDate($date)
    {
        return $this->setData(self::DATE, $date);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Kowal\UpdateStock\Api\Data\PrzyjecieExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\UpdateStock\Api\Data\PrzyjecieExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get sku
     * @return string|null
     */
    public function getSku()
    {
        return $this->_get(self::SKU);
    }

    /**
     * Set sku
     * @param string $sku
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * Get ilosc
     * @return string|null
     */
    public function getIlosc()
    {
        return $this->_get(self::ILOSC);
    }

    /**
     * Set ilosc
     * @param string $ilosc
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface
     */
    public function setIlosc($ilosc)
    {
        return $this->setData(self::ILOSC, $ilosc);
    }

    /**
     * Get magazyn
     * @return string|null
     */
    public function getMagazyn()
    {
        return $this->_get(self::MAGAZYN);
    }

    /**
     * Set magazyn
     * @param string $magazyn
     * @return \Kowal\UpdateStock\Api\Data\PrzyjecieInterface
     */
    public function setMagazyn($magazyn)
    {
        return $this->setData(self::MAGAZYN, $magazyn);
    }
}
