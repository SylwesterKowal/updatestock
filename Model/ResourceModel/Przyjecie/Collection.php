<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Model\ResourceModel\Przyjecie;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'przyjecie_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Kowal\UpdateStock\Model\Przyjecie::class,
            \Kowal\UpdateStock\Model\ResourceModel\Przyjecie::class
        );
    }
}

