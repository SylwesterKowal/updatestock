<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Model;

use Kowal\UpdateStock\Api\Data\PrzyjecieInterfaceFactory;
use Kowal\UpdateStock\Api\Data\PrzyjecieSearchResultsInterfaceFactory;
use Kowal\UpdateStock\Api\PrzyjecieRepositoryInterface;
use Kowal\UpdateStock\Model\ResourceModel\Przyjecie as ResourcePrzyjecie;
use Kowal\UpdateStock\Model\ResourceModel\Przyjecie\CollectionFactory as PrzyjecieCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;


class PrzyjecieRepository implements PrzyjecieRepositoryInterface
{

    protected $searchResultsFactory;

    private $collectionProcessor;

    protected $przyjecieCollectionFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $resource;

    private $storeManager;

    protected $extensionAttributesJoinProcessor;

    protected $przyjecieFactory;

    protected $dataPrzyjecieFactory;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourcePrzyjecie $resource
     * @param PrzyjecieFactory $przyjecieFactory
     * @param PrzyjecieInterfaceFactory $dataPrzyjecieFactory
     * @param PrzyjecieCollectionFactory $przyjecieCollectionFactory
     * @param PrzyjecieSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\InventoryApi\Api\Data\SourceItemInterfaceFactory $sourceItemFactory
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\InventoryApi\Api\Data\SourceItemInterface $sourceItemInterface
     * @param \Magento\InventoryApi\Api\SourceItemRepositoryInterface $sourceItemRepositoryInterface
     */
    public function __construct(
        ResourcePrzyjecie                        $resource,
        PrzyjecieFactory                         $przyjecieFactory,
        PrzyjecieInterfaceFactory                $dataPrzyjecieFactory,
        PrzyjecieCollectionFactory               $przyjecieCollectionFactory,
        PrzyjecieSearchResultsInterfaceFactory   $searchResultsFactory,
        DataObjectHelper                         $dataObjectHelper,
        DataObjectProcessor                      $dataObjectProcessor,
        StoreManagerInterface                    $storeManager,
        CollectionProcessorInterface             $collectionProcessor,
        JoinProcessorInterface                   $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter            $extensibleDataObjectConverter

    ) {
        $this->resource = $resource;
        $this->przyjecieFactory = $przyjecieFactory;
        $this->przyjecieCollectionFactory = $przyjecieCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPrzyjecieFactory = $dataPrzyjecieFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;

    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Kowal\UpdateStock\Api\Data\PrzyjecieInterface $przyjecie
    )
    {
        /* if (empty($przyjecie->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $przyjecie->setStoreId($storeId);
        } */

        $przyjecieData = $this->extensibleDataObjectConverter->toNestedArray(
            $przyjecie,
            [],
            \Kowal\UpdateStock\Api\Data\PrzyjecieInterface::class
        );

        $przyjecieModel = $this->przyjecieFactory->create()->setData($przyjecieData);

        try {
            $this->resource->save($przyjecieModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the przyjecie: %1',
                $exception->getMessage()
            ));
        }
        return $przyjecieModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($przyjecieId)
    {
        $przyjecie = $this->przyjecieFactory->create();
        $this->resource->load($przyjecie, $przyjecieId);
        if (!$przyjecie->getId()) {
            throw new NoSuchEntityException(__('Przyjecie with id "%1" does not exist.', $przyjecieId));
        }
        return $przyjecie->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    )
    {
        $collection = $this->przyjecieCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Kowal\UpdateStock\Api\Data\PrzyjecieInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Kowal\UpdateStock\Api\Data\PrzyjecieInterface $przyjecie
    )
    {
        try {
            $przyjecieModel = $this->przyjecieFactory->create();
            $this->resource->load($przyjecieModel, $przyjecie->getPrzyjecieId());

            $this->resource->delete($przyjecieModel);


        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Przyjecie: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($przyjecieId)
    {
        return $this->delete($this->get($przyjecieId));
    }



}

