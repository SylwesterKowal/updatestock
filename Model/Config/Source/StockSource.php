<?php
namespace Kowal\UpdateStock\Model\Config\Source;

class StockSource implements \Magento\Framework\Data\OptionSourceInterface
{
    private $sourceRepository;
    public function __construct(

        \Magento\InventoryApi\Api\SourceRepositoryInterface $sourceRepository

    ) {
        $this->sourceRepository = $sourceRepository;
    }

    public function getSourcesList()
    {
        $sourceData = $this->sourceRepository->getList();
        return $sourceData->getItems();
    }
    public function toOptionArray()
    {
        $sourceList = $this->getSourcesList();
        $op = [];
        foreach ($sourceList as $source) {
            $op[] = ['value' => $source->getData('source_code'), 'label'=>$source->getData('name')];
        }

        return $op;

//        return [
//            ['value' => 'default', 'label' => __('Default')],
//            ['value' => 'dropshipping', 'label' => __('Dropshipping')]
//        ];
    }
}
