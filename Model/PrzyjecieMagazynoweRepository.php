<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Model;

use Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterfaceFactory;
use Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweSearchResultsInterfaceFactory;
use Kowal\UpdateStock\Api\PrzyjecieMagazynoweRepositoryInterface;
use Kowal\UpdateStock\Model\ResourceModel\PrzyjecieMagazynowe as ResourcePrzyjecieMagazynowe;
use Kowal\UpdateStock\Model\ResourceModel\PrzyjecieMagazynowe\CollectionFactory as PrzyjecieMagazynoweCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class PrzyjecieMagazynoweRepository implements PrzyjecieMagazynoweRepositoryInterface
{

    protected $przyjecieMagazynoweFactory;

    protected $resource;

    private $storeManager;

    protected $dataPrzyjecieMagazynoweFactory;

    protected $przyjecieMagazynoweCollectionFactory;

    protected $dataObjectProcessor;

    protected $dataObjectHelper;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;


    /**
     * @param ResourcePrzyjecieMagazynowe $resource
     * @param PrzyjecieMagazynoweFactory $przyjecieMagazynoweFactory
     * @param PrzyjecieMagazynoweInterfaceFactory $dataPrzyjecieMagazynoweFactory
     * @param PrzyjecieMagazynoweCollectionFactory $przyjecieMagazynoweCollectionFactory
     * @param PrzyjecieMagazynoweSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePrzyjecieMagazynowe $resource,
        PrzyjecieMagazynoweFactory $przyjecieMagazynoweFactory,
        PrzyjecieMagazynoweInterfaceFactory $dataPrzyjecieMagazynoweFactory,
        PrzyjecieMagazynoweCollectionFactory $przyjecieMagazynoweCollectionFactory,
        PrzyjecieMagazynoweSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->przyjecieMagazynoweFactory = $przyjecieMagazynoweFactory;
        $this->przyjecieMagazynoweCollectionFactory = $przyjecieMagazynoweCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPrzyjecieMagazynoweFactory = $dataPrzyjecieMagazynoweFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface $przyjecieMagazynowe
    ) {
        /* if (empty($przyjecieMagazynowe->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $przyjecieMagazynowe->setStoreId($storeId);
        } */

        $przyjecieMagazynoweData = $this->extensibleDataObjectConverter->toNestedArray(
            $przyjecieMagazynowe,
            [],
            \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface::class
        );

        $przyjecieMagazynoweModel = $this->przyjecieMagazynoweFactory->create()->setData($przyjecieMagazynoweData);

        try {
            $this->resource->save($przyjecieMagazynoweModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the przyjecieMagazynowe: %1',
                $exception->getMessage()
            ));
        }
        return $przyjecieMagazynoweModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($przyjecieMagazynoweId)
    {
        $przyjecieMagazynowe = $this->przyjecieMagazynoweFactory->create();
        $this->resource->load($przyjecieMagazynowe, $przyjecieMagazynoweId);
        if (!$przyjecieMagazynowe->getId()) {
            throw new NoSuchEntityException(__('PrzyjecieMagazynowe with id "%1" does not exist.', $przyjecieMagazynoweId));
        }
        return $przyjecieMagazynowe->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->przyjecieMagazynoweCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Kowal\UpdateStock\Api\Data\PrzyjecieMagazynoweInterface $przyjecieMagazynowe
    ) {
        try {
            $przyjecieMagazynoweModel = $this->przyjecieMagazynoweFactory->create();
            $this->resource->load($przyjecieMagazynoweModel, $przyjecieMagazynowe->getPrzyjeciemagazynoweId());
            $this->resource->delete($przyjecieMagazynoweModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the PrzyjecieMagazynowe: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($przyjecieMagazynoweId)
    {
        return $this->delete($this->get($przyjecieMagazynoweId));
    }
}
