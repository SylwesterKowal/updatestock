<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\UpdateStock\Model;

use Kowal\UpdateStock\Api\Data\PrzyjecieInterface;
use Kowal\UpdateStock\Api\Data\PrzyjecieInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Przyjecie extends \Magento\Framework\Model\AbstractModel
{

    protected $przyjecieDataFactory;

    protected $_eventPrefix = 'kowal_updatestock_przyjecie';
    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PrzyjecieInterfaceFactory $przyjecieDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Kowal\UpdateStock\Model\ResourceModel\Przyjecie $resource
     * @param \Kowal\UpdateStock\Model\ResourceModel\Przyjecie\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PrzyjecieInterfaceFactory $przyjecieDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Kowal\UpdateStock\Model\ResourceModel\Przyjecie $resource,
        \Kowal\UpdateStock\Model\ResourceModel\Przyjecie\Collection $resourceCollection,
        array $data = []
    ) {
        $this->przyjecieDataFactory = $przyjecieDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve przyjecie model with przyjecie data
     * @return PrzyjecieInterface
     */
    public function getDataModel()
    {
        $przyjecieData = $this->getData();
        
        $przyjecieDataObject = $this->przyjecieDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $przyjecieDataObject,
            $przyjecieData,
            PrzyjecieInterface::class
        );
        
        return $przyjecieDataObject;
    }
}

